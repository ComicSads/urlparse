/*
My non-standard way of storing urls
*/
package urlparse

import (
	"strings"
)

// A map of all the prefixes being used by parse function
var Prefixes = map[string]string {
	"youtube://": "https://youtube.com/watch?v=",
}

// Parse parses a non-standard url to what it actually is reffering to.
// Usually things like youtube:// to the actual youtube video url.
// When no prefix is found, Parse returns original string.
func Parse(s string) string {
	for k, v := range Prefixes {
		if strings.HasPrefix(s, k) {
			return v + strings.TrimPrefix(s, k)
		}
	}
	return s
}

// Encode does the opposite of Parse, encoding a string to use a Prefix
// When no available prefix is found, Encode returns original string
func Encode(s string) string {
	for k,v := range Prefixes{
		if strings.HasPrefix(s, v) {
			return k + strings.TrimPrefix(s, v)
		}
	}
	return s
}
